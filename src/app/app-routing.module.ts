import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './main/login/login.component';


const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('src/app/main/login/login.module').then(m => m.LoginModule),
},
{ path: 'product', loadChildren: () => import('src/app/main/product/product.module').then(m => m.ProductModule) },
  {
    path: '',
    redirectTo: '/login',
   pathMatch: 'full'
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule,]
})
export class AppRoutingModule { }
