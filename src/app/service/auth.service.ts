import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
};

constructor(private http: HttpClient) {}

login(authData): Observable<any> {
  console.log(authData)
  return this.http.post<any>("http://localhost:3000/login",authData)
}
}
