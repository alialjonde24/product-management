import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {}

  getProduct(page,size): Observable<any[]> {
    return this.http.get<any[]>(`http://localhost:3000/product/pageSize=${page}&pageNumber=${size}`)
  }
  deleteProduct(id): Observable<any[]> {
    return this.http.delete<any[]>(`http://localhost:3000/product/${id}`)
  }
  updateProduct(id,product): Observable<any[]> {
    return this.http.put<any[]>(`http://localhost:3000/product/${id}`,product)
  }
}
