import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { RouterModule } from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import { ProductDetailsComponent } from './dialog/product-details/product-details.component';
import { ProductEditeComponent } from './dialog/product-edite/product-edite.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
@NgModule({
  declarations: [ProductComponent, ProductDetailsComponent, ProductEditeComponent],
  imports: [
     RouterModule.forChild([
      {
        path: '',
        component: ProductComponent
      }]),
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule, 
    MatPaginatorModule,
    MatSelectModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatProgressSpinnerModule
  ],
  entryComponents:[ProductDetailsComponent,ProductEditeComponent]
})
export class ProductModule { }
