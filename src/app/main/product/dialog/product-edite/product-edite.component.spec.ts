import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductEditeComponent } from './product-edite.component';

describe('ProductEditeComponent', () => {
  let component: ProductEditeComponent;
  let fixture: ComponentFixture<ProductEditeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductEditeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductEditeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
