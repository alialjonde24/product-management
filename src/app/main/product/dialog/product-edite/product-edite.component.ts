import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-product-edite',
  templateUrl: './product-edite.component.html',
  styleUrls: ['./product-edite.component.css']
})
export class ProductEditeComponent implements OnInit {
  Edite: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ProductEditeComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder) {}

  ngOnInit(): void {
    this.Edite = this.fb.group({
      title   : [this.data.title, [Validators.required ]],
      description: [this.data.description, Validators.required],
      price: [this.data.price, Validators.required],
      quantity: [this.data.quantity, Validators.required],
      image:[this.data.image, Validators.required],
      _id:[this.data._id, Validators.required]
    });
  }
  update(values){
    console.log(values)
  }
}
