import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ProductService } from 'src/app/service/product.service';
import { ProductDetailsComponent } from './dialog/product-details/product-details.component';
import { ProductEditeComponent } from './dialog/product-edite/product-edite.component';
import jwt_decode from 'jwt-decode';
@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(public dialog: MatDialog,private ProductService:ProductService) { }
  products
  dataSize
  size
  page
  loading=false
  admin=false
  ngOnInit(): void {
 
   let user= getDecodedAccessToken(localStorage.getItem('admin'))
    if ( user==null) {
      this.admin=false
    } else {
      if (user.role=="admin") {
        this.admin=true
      }
    }
 
    this.GetData(5,0)
  }
  openDetailsDialog(product){
    const dialogRef = this.dialog.open(ProductDetailsComponent,{
      data: {title: product.title,image:product.image,description:product.description,price:product.price,quantity:product.quantity}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  
  edite(product,event){
    switch (event) {
      case "delete":
        if(confirm(`delete ${product.title}`)){
          this.loading=true
          this.ProductService.deleteProduct(product._id).subscribe(data=>{
            console.log(data)
            if (data['message']=="deleted successfully") {
              
              this.GetData(this.size,this.page)
            }
          })
        }
        break; 
      case "edite":
        const dialogRef = this.dialog.open(ProductEditeComponent,{
          data: {title: product.title,image:product.image,description:product.description,price:product.price,quantity:product.quantity,_id:product._id}
        });
        dialogRef.afterClosed().subscribe(result => {
          console.log(result)
          if (result != 'close') {
            this.loading=true
            this.ProductService.updateProduct(result._id,result).subscribe(data=>{
              console.log(data)  
                this.GetData(this.size,this.page)
            })
          }

        });
        break;
    }
    console.log(product,event)
  }

  GetData(size,page){
    this.loading=true
    this.size=size;
    this.page=page
    this.ProductService.getProduct(size,page).subscribe(data=>{
      this.products=data['data']
      this.dataSize=data['count']
      this.loading=false
    })
  }

  changepage(event){
    this.GetData(event.pageSize,event.pageIndex)
    console.log(event)
  }

  logout(){
    localStorage.removeItem("admin");
  }
}
 function getDecodedAccessToken(token: string): any {
  try{
      return jwt_decode(token);
  }
  catch(Error){
      return null;
  }
}