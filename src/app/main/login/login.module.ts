import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { AuthService } from 'src/app/service/auth.service';
const routes = [
  {
      path     : '**',
      component: LoginComponent
  }
];


@NgModule({
  declarations: [LoginComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MatIconModule,
    FormsModule, ReactiveFormsModule
  ],
  //providers:[AuthService]
})
export class LoginModule { }
