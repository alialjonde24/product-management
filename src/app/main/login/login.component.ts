import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor( private formBuilder: FormBuilder,
    private AuthService:AuthService,
    private _router: Router,
    ) { }
  Loginform: FormGroup;
  formError=[false,false]
  message=''
  ngOnInit(): void {
    this.Loginform = this.formBuilder.group({
      username   : ['', [Validators.required ]],
      password: ['', Validators.required]
    });
  }
  login(){
    if (this.Loginform.invalid) {
      this.formError[0]=this.Loginform.controls.username.invalid
      this.formError[1]=this.Loginform.controls.password.invalid
    } else {
      this.formError[0]=this.Loginform.controls.username.invalid
      this.formError[1]=this.Loginform.controls.password.invalid
      this.AuthService.login(this.Loginform.value).subscribe(data=>{
      if (data.accessToken == undefined) {
        this.message=data.message
      }
      else{
        this.message=''
        localStorage.setItem('admin', JSON.stringify(data.accessToken))
        this._router.navigate(['/product']);
      }
     })
    }
  }

  userLogin(){
    localStorage.removeItem("admin");
  }

}
